/**
 * @description       : 
 * @last modified on  : 10-21-2021
 * @last modified by  : Alejandro Hernández
**/
trigger OrderTrigger on Order (before insert, after insert, after update) {
    Map<Id, Contact> clients;
    AggregateResult[] totalAmountOfOrdersPerStore;
    Map<Id, Account> stores;
    Set<Id> associatedClientsId = new Set<Id>();
    List<Account> accountRanking = new List<Account>();
    Map<Id, Integer> totalAmountSpentByCustomer = new Map<Id, Integer>();


    /*
     _         _ _     _          __             
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___ 
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___|  
    */        
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            OrderTriggerHandler.fillAssociatedClientsSet(associatedClientsId);
            clients = new Map<Id, Contact>([SELECT FirstName, LastName, Id FROM Contact WHERE Id IN :associatedClientsId]);
        }
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if (Trigger.isInsert) {
            for (AggregateResult ar : [SELECT Cliente__c, SUM(TotalAmount)Amount FROM Order WHERE Cliente__c != null GROUP BY Cliente__c]) {
                totalAmountSpentByCustomer.put((Id) ar.get('Cliente__c'), Integer.valueOf(ar.get('Amount')));
            }
        }

        if(Trigger.isInsert || Trigger.isUpdate) {
            totalAmountOfOrdersPerStore = [SELECT Tienda__c, SUM(TotalAmount)Amount FROM Order WHERE Tienda__c != null GROUP BY Tienda__c ORDER BY SUM(TotalAmount) DESC];
            stores = new Map<Id, Account>([SELECT Id, Ranking__c FROM Account WHERE CIF__c != null]);
        }
    }
    

    /*                         _           _   _             
    _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
    | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
    |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */


    for(Order order : (List<Order>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
                OrderTriggerHandler.informUniqueKeyFieldFromOrder(order, clients);
            }
            if(Trigger.isUpdate) {}
        }
        
        if (Trigger.isAfter) {
            if (Trigger.isUpdate) {
                if (Trigger.new.size() == 1) {
                    if (order.Estado__c == 'Cerrado') {
                        OrderTriggerHandler.generatePdfWithOrderSummary(String.valueOf(order.Id));
                    } else if (order.Estado__c == 'Pagado') {
                        OrderTriggerHandler.sendEmailToClientWithOrderPDF(order);
                    }
                }
            }

            if (Trigger.isInsert) {
                OrderTriggerHandler.newEmailToClient(order, totalAmountSpentByCustomer);
            }
        }
    }
    
    
    /*
    _         _ _           __ _           
    | |__ _  _| | |__  __ _ / _| |_ ___ _ _ 
    | '_ \ || | | / / / _` |  _|  _/ -_) '_|
    |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_|  
    */
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }
    
    if(Trigger.isAfter) {
        if(Trigger.isInsert || Trigger.isUpdate) {
            OrderTriggerHandler.calculateRanking(totalAmountOfOrdersPerStore, accountRanking, stores);
            update accountRanking;
        }
    }
}