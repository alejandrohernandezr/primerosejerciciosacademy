/**
 * @description       : 
 * @last modified on  : 10-21-2021
 * @last modified by  : Alejandro Hernández
**/
trigger BoardgameTrigger on Boardgame__c (after insert, after update, before insert) {
    List<Product2> newProducts = new List<Product2>();
    Set<String> alreadyExistingProducts = new Set<String>();
     
    /*     _ _     _          __             
        | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___ 
        | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
        |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___|  
        */        
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
            }

            if(Trigger.isUpdate) {}
        }

        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                for (Product2 p : [SELECT Boardgame__r.BGG_id__c FROM Product2]) {
                    alreadyExistingProducts.add(p.Boardgame__r.BGG_id__c);
                }
            }

            if(Trigger.isUpdate) {}
        }

        /*                         _           _   _             
             _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
            | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
            |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
        */
        for(Boardgame__c bg : (List<Boardgame__c>) Trigger.new) {
            if(Trigger.isBefore) {
                if(Trigger.isInsert) {
                }
            }

            if(Trigger.isAfter) {
                if(Trigger.isInsert) {
                    BoardgameTriggerHandler.associateProductWithNewlyCreatedBoardgame(bg, alreadyExistingProducts, newProducts);
                    BoardgameTriggerHandler.retrieveInfoFromBGGApi(bg.Name, bg.Id);
                }
            }
        }


        /*
             _         _ _           __ _           
            | |__ _  _| | |__  __ _ / _| |_ ___ _ _ 
            | '_ \ || | | / / / _` |  _|  _/ -_) '_|
            |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_|  
        */
		
		if(Trigger.isBefore) {
            if(Trigger.isInsert) {}
            if(Trigger.isUpdate) {}
        }

        if(Trigger.isAfter) {

            if(Trigger.isInsert) {
                insert newProducts;
            }

            if(Trigger.isUpdate) {}
        }
}