/**
 * @description       : 
 * @last modified on  : 10-19-2021
 * @last modified by  : Alejandro Hernández
**/
trigger ContactTrigger on Contact (before delete, before insert, before update, after delete, after insert, after update) {

    Map<Id, Account> storeMap = new Map<Id, Account>();
    Map<Id, Integer> numberOfClientsPerStore = new Map<Id, Integer>();
    Set<Contact> clientsWithPreferredStore;

    /*
     _         _ _     _          __             
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___ 
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___|  
    */        
    if(Trigger.isBefore) {
        if(Trigger.isInsert || Trigger.isUpdate) {
            clientsWithPreferredStore = new Set<Contact>([SELECT Id, Preferred_Store__c FROM Contact WHERE Preferred_Store__c != null]);
            
            for (Contact contact : (List<Contact>) Trigger.new) {
                if (contact.Preferred_Store__c != null) {
                    clientsWithPreferredStore.add(contact);
                }
            }
            
            storeMap = new Map<Id,Account>([SELECT Id, Name, BillingCity FROM Account WHERE CIF__c != null AND RecordType.Name = 'Física']);
            ContactTriggerHandler.getHowManyClientsHasEachStore(numberOfClientsPerStore, clientsWithPreferredStore, storeMap);
        }
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }
        

    /*                         _           _   _             
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(Contact contact : (List<Contact>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isInsert || Trigger.isUpdate) {
                ContactTriggerHandler.checkIfStoreAndClientCityAreTheSame(contact, numberOfClientsPerStore, storeMap);
            }
        }

        if(Trigger.isAfter) {
            if(Trigger.isInsert) {}
            if(Trigger.isUpdate) {}
        }
    }


    /*
         _         _ _           __ _           
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _ 
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_|  
    */
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }
}