/**
 * @description       : 
 * @last modified on  : 10-21-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class BoardgameTriggerHandler {
    public BoardgameTriggerHandler() {
        
    }

    public static void associateProductWithNewlyCreatedBoardgame(Boardgame__c bg, Set<String> alreadyExistingProducts, List<Product2> newProducts) {
        Product2 product = new Product2();
        String boardgameBGGId = bg.BGG_id__c;

        if (!alreadyExistingProducts.contains(boardgameBGGId)) {
            product.Name = bg.Name;
            product.ProductCode = 'BG'+boardgameBGGId;
            product.IsActive = true;
            product.Boardgame__c = bg.Id;

            alreadyExistingProducts.add(boardgameBGGId);
            newProducts.add(product);
        }
    }

    @Future(callout=true)
    public static void retrieveInfoFromBGGApi(String query, String id) {
        String queryFormed = query.replaceAll(' ', '+');
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String gameName = '';
        String apiUrl =  'https://boardgamegeek.com/xmlapi2/search/?query="' + queryFormed + '"';
        Boardgame__c boardgame = new Boardgame__c();
        boardgame.Id = id;
        String bggId;

        req.setEndpoint(apiUrl);
        req.setMethod('GET');
        HttpResponse resp = http.send(req);
        System.debug(resp.getBody());
        Dom.Document doc = resp.getBodyDocument();
        Dom.XmlNode games = doc.getRootElement();
        
        System.debug(doc);
        for (Dom.XmlNode game : games.getChildElements()) {
            gameName = game.getChildElement('name', null).getAttribute('value', null);

            if (gameName == query) {
                bggId = game.getAttribute('id', null);

                break;
            }
        }


        if (bggId != null) {
            boardgame.BGG_id__c = bggId;

            update boardgame;
        } else {
            boardgame.addError('Couldn\t find any game with that name. Please try again.');
        }
    }

}
