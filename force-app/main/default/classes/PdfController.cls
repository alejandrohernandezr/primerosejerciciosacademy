/**
 * @description       : 
 * @last modified on  : 10-20-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class PdfController {
    private Id orderId;
    public Order order {get; set;}
    public PdfController (){
        orderId = ApexPages.currentPage().getParameters().get('Id');
        order = [SELECT Cliente__r.FirstName, Cliente__r.LastName, OrderNumber, TotalAmount FROM Order WHERE Id = :orderId];
    }
}