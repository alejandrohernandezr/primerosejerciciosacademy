/**
 * @description       : 
 * @last modified on  : 11-11-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class Academy {
    public Academy() {
        
    }

    // Ejercicio 2
    public Map<String, Boardgame__c> getProductIdsAndAssociateBoardgameRecords() {
        Map<String, Boardgame__c> idsAndRecord = new Map<String, Boardgame__c>();
        List<Boardgame__c> bgs = [SELECT BGG_id__c, Calificacion__c, Categoria__c, CreatedById, CreatedDate, Editorial__c, Id, IsDeleted, LastModifiedById, LastModifiedDate, LastReferencedDate, LastViewedDate, Name, Nota__c, OwnerId, Subcategoria__c, SystemModstamp
                                FROM Boardgame__c];
        Boardgame__c tmp;

        Map<String, Boardgame__c> boardgames = new Map<String, Boardgame__c>();
        for (Boardgame__c bg : bgs) 
            boardgames.put(bg.Name, bg);

        for (Product2 product : [SELECT Id, Name FROM Product2]) {
            tmp = boardgames.get(product.Name);

            if (tmp != null) 
                idsAndRecord.put(String.valueOf(product.Id), tmp);
            
        }

        System.debug(JSON.serializePretty(idsAndRecord));
        return idsAndRecord;
    }
 
    // Ejercicio 3
    public Map<String, Double> avgCalificationPerCategory() {
        List<Boardgame__c> categories = [SELECT Categoria__c FROM Boardgame__c];
        Map<String, Double> notas = new Map<String, Double>();
        Double average = 0;

        for (Boardgame__c b : categories) {
            List<Boardgame__c> califications = [SELECT Nota__c FROM Boardgame__c WHERE Categoria__c = :b.Categoria__c];

            if (!califications.isEmpty()) {
                average = 0;
                
                for (Boardgame__c boardgame: califications)
                    average += boardgame.Nota__c != null ? boardgame.Nota__c : 0;

                average = average/califications.size();
            }

            notas.put(String.valueOf(b.Categoria__c), average);    
        }

        System.debug(JSON.serializePretty(notas));

        return notas;
    }

    // Ejercicio 3* (AVG y AggregateResult)
    public Map<String, Double> avgCalificationPerCategoryNewVersion() {
        Map<String, Double> califications = new Map<String, Double>();
    
        AggregateResult[] notesByCategory = [SELECT AVG(Nota__c)average, Categoria__c FROM Boardgame__c GROUP BY Categoria__c];
        
        for (AggregateResult ar : notesByCategory) {
            califications.put(String.valueOf(ar.get('Categoria__c')), (Double) ar.get('average'));
        }

        System.debug(JSON.serializePretty(califications));
        return califications;
    }


    // Ejercicio 4
    public static void relateEachClientWithAPreferredStore() {
        Map<Id, Account> storeMap = new Map<Id, Account>();
        Map<Id, Integer> numberOfRelatedCustomers = new Map<Id, Integer>();
        List<Contact> unassignedClients = [SELECT Id, Name FROM Contact WHERE Preferred_Store__c = null];
        List<Contact> updateList = new List<Contact>();
        // unassociateClientsToStores();

        for (Account a : [SELECT Id, Name, CIF__c FROM Account WHERE CIF__c != null AND RecordType.Name = 'Física']) {
            storeMap.put(a.Id, a);
            numberOfRelatedCustomers.put(a.Id, 0);
        }       

        // System.debug(JSON.serializePretty(storeMap));
        // System.debug('#######################################################');
        // System.debug(JSON.serializePretty(numberOfRelatedCustomers));

        for (Contact c : [SELECT Id, Name, Preferred_Store__c FROM Contact WHERE Preferred_Store__c != null]) {
            numberOfRelatedCustomers.put(c.Preferred_Store__c, numberOfRelatedCustomers.get(c.Preferred_Store__c)+1);
        }

        for (Contact c : unassignedClients) {
            Id insertToStore = getStoreWithLessRelatedCustomers(numberOfRelatedCustomers);
            c.Preferred_Store__c = insertToStore;
            updateList.add(c);
            Integer val = numberOfRelatedCustomers.get(insertToStore);
            val = val + 1;
            numberOfRelatedCustomers.put(insertToStore, val);
        }

        update updateList;
    }

    private static Id getStoreWithLessRelatedCustomers(Map<Id, Integer> relatedCustomers) {
        Id minId;
        Integer min = 10000000;

        for (Id id : relatedCustomers.keySet()) {
            if (relatedCustomers.get(id) < min) {
                min = relatedCustomers.get(id);
                minId = id;
            }
        }

        return minId;
    }

    /**
    * @description 
    * @author Alejandro Hernández | 10-13-2021 
    **/
    private static void unassociateClientsToStores() {
        List<Contact> updateList = new List<Contact>();
        for (Contact c : [SELECT Preferred_Store__c FROM Contact]) {
            c.Preferred_Store__c = null;
            updateList.add(c);
        }

        update updateList;
    }

    
    /**
    * @description 
    * @author Alejandro Hernández | 10-13-2021 
    **/
    public static void sendEmailToCustomersWithPreferredStoreAndOrders() {
        Map<Id,List<Order>> clientIdOrdersMap = new Map<Id,List<Order>>();
        Map<Id, Account> storeMap = new Map<Id, Account>();

        // Recorrer los orders
        for (Order order : [SELECT OrderNumber, Cliente__c, TotalAmount, EffectiveDate, Tienda__c, ShippingAddress FROM Order]) {
            List<Order> clientOrders = clientIdOrdersMap.containsKey(order.Cliente__c) ? clientIdOrdersMap.get(order.Cliente__c) : new List<Order>();
            clientOrders.add(order);
            clientIdOrdersMap.put(order.Cliente__c,clientOrders);
        }

        for (Account store : [SELECT Id, Name, CIF__c FROM Account WHERE CIF__c != null]) {
            storeMap.put(store.Id, store);
        }

        for (Contact client : [SELECT Name, Email, Preferred_Store__c FROM Contact WHERE Preferred_Store__c != null]) {
            Account store = storeMap.get(client.Preferred_Store__c);
            sendEmail(client.Email, 'Mucho AP - Tu tienda favorita', store, clientIdOrdersMap.get(client.Id));
        }

        System.debug(JSON.serializePretty(clientIdOrdersMap));
    
    }

    /**
    * @description 
    * @author Alejandro Hernández | 10-13-2021 
    * @param address Where to send the email
    * @param subject Subject of the email
    * @param acc Account (Store) object
    * @param orders Collection of orders from the user
    **/
    private static void sendEmail(String address, String subject, Account acc, List<Order> orders) {
        String body = '';
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        email.setToAddresses(new List<String>{address});
        email.setSubject(subject);

        if (acc != null) {
            body += 'Nombre de la tienda: ' + acc.Name + '<br>';
            body += 'CIF: ' + acc.CIF__c + '<br>';
        } else { 
            body = 'No tienes ninguna tienda favorita aún. <br>';
        }

        if (orders != null) {
            body += '<h4>Tus pedidos</h4>';

            for (Order o : orders) {
                body += '<h5>Pedido #'+o.Id+'</h5>';
                body += '<p>Fecha ' + o.EffectiveDate + '</p>';
                body += '<hr>';
            }
        }

        email.setPlainTextBody(body);
        email.setHtmlBody(body);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }

    
    /**
    * @description 
    * @author Alejandro Hernández | 10-13-2021 
    * @param username Username to check its own boardgames
    * @return List<Boardgame__c> 
    **/
    @Future(callout=true)
    public static void retrieveAllBoardgamesFromBGGUser(String username) {
        List<Boardgame__c> boardgamesFromUser = new List<Boardgame__c>();
        Set<String> bgSet = new Set<String>();
        Map<String, Boardgame__c> boardgames = new Map<String, Boardgame__c>();
        List<Boardgame__c> newBoardgames = new List<Boardgame__c>();
        String apiUrl = 'https://boardgamegeek.com/xmlapi2/collection?stats=1&own=1&username=';

        for (Boardgame__c bg : [SELECT BGG_id__c, Name, Id FROM Boardgame__c]) {
            boardgames.put(bg.BGG_id__c, bg);
        }

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(apiUrl+'divad_cr');
        req.setMethod('GET');
        HttpResponse resp = http.send(req);
        Dom.Document doc = resp.getBodyDocument();

        // esto es 'items'
        Dom.XMLNode games = doc.getRootElement();

        for (Dom.XMLNode game : games.getChildElements()) {
            String gameId = game.getAttribute('objectid', null);
            Boardgame__c newBg = new Boardgame__c();

            newBg.Name = game.getChildElement('name', null).getText(); 
            newBg.BGG_id__c = gameId;
            Decimal note = Decimal.valueOf(game.getChildElement('stats', null)
                                    .getChildElement('rating', null)
                                    .getChildElement('average', null)
                                    .getAttribute('value', null));

            newBg.Nota__c = note.setScale(2);
            newBg.Editorial__c = 'Asmodee';
            newBg.Categoria__c = 'Filler';
            // newBg.Precio__c = Integer.valueof((Math.random() * 90) + 10);
            
            if (boardgames.get(gameId) == null && !bgSet.contains(gameId)) {
                newBoardgames.add(newBg);  
                bgSet.add(gameId);
            }

            boardgamesFromUser.add(newBg);
        }
        
        upsert newBoardgames BGG_id__c;

        // return boardgamesFromUser;
    }
    
}