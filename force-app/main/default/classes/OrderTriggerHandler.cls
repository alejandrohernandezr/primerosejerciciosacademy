/**
 * @description       : 
 * @last modified on  : 10-20-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class OrderTriggerHandler {

    public static void informUniqueKeyFieldFromOrder(Order order, Map<Id, Contact> clients) {
        Contact client = clients.get(order.Cliente__c);

        if (client != null && order != null) {
            String datestamp = String.valueOf(DateTime.now().getTime());
            order.UniqueKey__c = client.FirstName + ' ' + client.LastName + ' ' + datestamp;
        }
    }

    public static void fillAssociatedClientsSet(Set<Id> associatedClients) {
        for (Order order : (List<Order>) Trigger.new) {
            associatedClients.add(order.Cliente__c);
        }
    }

    public static void calculateRanking(AggregateResult[] totalAmountPerStore, List<Account> ranking, Map<Id,Account> stores) {
        Integer i = 1;
        for (AggregateResult ar : totalAmountPerStore) {
            Account store = stores.get((Id) ar.get('Tienda__c'));
            store.Ranking__c = i;
            ranking.add(store);

            i++;
        }
    }

    @Future(callout=true)
    public static void generatePdfWithOrderSummary(String orderId) {
        Pagereference page = new Pagereference('/apex/GeneratePdf');
        ContentDocument pdf;
        ContentVersion pdfCv = new ContentVersion();
        ContentDocumentLink link = new ContentDocumentLink();
        Id contentDocumentId;
        
        page.getParameters().put('Id', orderId);
        pdfCv.Title = 'Order-' + orderId + '-Summary';
        pdfCv.PathOnClient = pdfCv.Title+'.pdf';
        pdfCv.VersionData = page.getContentAsPDF();
        pdfCv.IsMajorVersion = true;
        insert pdfCv;

        contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :pdfCv.Id].ContentDocumentId;
        link.LinkedEntityId = orderId;
        link.ContentDocumentId = contentDocumentId;
        link.ShareType = 'V';
        insert link;
    }

    public static void sendEmailToClientWithOrderPDF(Order order) {
        String clientEmail = [SELECT Email FROM Contact WHERE Id = :order.Cliente__c].Email;
        Id contentDocumentId = [SELECT id, ContentDocument.id, LinkedEntityId FROM Contentdocumentlink WHERE LinkedEntityId = :order.Id].ContentDocument.id;
        Id cvId = [SELECT LatestPublishedVersion.Id FROM ContentDocument WHERE Id = :contentDocumentId].LatestPublishedVersion.Id;
        ContentVersion attachment = [SELECT PathOnClient, VersionData FROM ContentVersion WHERE Id = :cvId];
        
        if (clientEmail != null) {
            OrderTriggerHandler.sendEmail(clientEmail, 'Pedido', attachment);
        } else {
            order.addError('Client email address is empty.');
        }
    }

    public static void sendEmail(String address, String subject, ContentVersion cv) {
        String body = '';
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        email.setToAddresses(new List<String>{address});
        email.setSubject(subject);

        Messaging.EmailFileAttachment file = new Messaging.EmailFileAttachment();
        file.setFileName(cv.PathOnClient);
        file.setBody(cv.VersionData);        

        email.setPlainTextBody(body);
        email.setHtmlBody(body);

        email.setFileAttachments(new Messaging.EmailFileAttachment[] {file});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }

    public static void newEmailToClient(Order order, Map<Id, Integer> totalAmountSpentByCustomer) {
        String clientEmail = [SELECT Email FROM Contact WHERE Id = :order.Cliente__c].Email;
        Email__c email = new Email__c();

        email.Address__c = clientEmail;
        email.Subject__c = 'Pedido - '+order.OrderNumber;
        email.Body__c = ' ';
        email.Total_Amount_Spent__c = totalAmountSpentByCustomer.get(order.Cliente__c);
        insert email;
    }
}
