/**
 * @description       : 
 * @last modified on  : 10-18-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class ProductTriggerHandler {
    public ProductTriggerHandler() {
    }

    public static void createPricebookEntryForNewProducts(List<Product2> newProducts, Id pricebookId, List<PricebookEntry> newPricebooks) {
        PricebookEntry pbe;

        for (Product2 p : newProducts) {
            pbe  = new PricebookEntry();
            pbe.Product2Id = p.Id;
            pbe.IsActive = true;
            // pbe.UseStandardPrice = true;
            pbe.UnitPrice = Integer.valueof((Math.random() * 45) + 5);
            pbe.Pricebook2Id = pricebookId;

            newPricebooks.add(pbe);
        }
    }

    public static void populateProductFieldFromBoardgameRecord(Product2 p, Map<Id, Boardgame__c> boardgames, List<Boardgame__c> toUpdate) {
        Boardgame__c bg = boardgames.get(p.Boardgame__c);

        if (bg != null) {
            bg.Product__c = p.Id;
            toUpdate.add(bg);
        }
    }
}