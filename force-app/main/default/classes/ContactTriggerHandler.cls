/**
 * @description       : 
 * @last modified on  : 10-19-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class ContactTriggerHandler {

    public static void checkIfStoreAndClientCityAreTheSame(Contact client, Map<Id, Integer> numberOfClientsPerStore, Map<Id, Account> storeMap) {
        Account store = storeMap.get(client.Preferred_Store__c);

        if (client.Preferred_Store__c == null) {
            Id minStore = getStoreWithLessCustomers(storeMap, numberOfClientsPerStore, client.MailingCity);
            client.Preferred_Store__c = minStore;            
        } else if (store.BillingCity != client.MailingCity) { 
            client.addError('Client city and preferred store city doesn\'t match');
        }
    }

    public static Id getStoreWithLessCustomers(Map<Id,Account> storeMap, Map<Id, Integer> numberOfClientsPerStore, String city) {
        Id minId;
        Integer min = 10000000;

        for (Id id : numberOfClientsPerStore.keySet()) {
            Account storeData = storeMap.get(Id);
            if (numberOfClientsPerStore.get(id) < min && storeData.BillingCity == city) {
                min = numberOfClientsPerStore.get(id);
                minId = id;
            }
        }

        return minId;
    }

    public static void getHowManyClientsHasEachStore(Map<Id, Integer> numberOfClientsPerStore,  Set<Contact> clients, Map<Id, Account> storeMap) {
        for (Id id : storeMap.keySet()) {
            numberOfClientsPerStore.put(id, 0);
        }

        for (Contact client : clients) {
            Integer nClients = numberOfClientsPerStore.containsKey(client.Preferred_Store__c) ? numberOfClientsPerStore.get(client.Preferred_Store__c) + 1 : 0;
            numberOfClientsPerStore.put(client.Preferred_Store__c, nClients);
        }
    }

}

